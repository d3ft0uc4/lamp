

var user_country = (YMaps.location.country);
var user_city    = (YMaps.location.city);

if (user_city) 
{
    city_delivery = 'г. '+user_city;
}  
else
{
    city_delivery = 'Ваш город';        
}  

jQuery(document).ready(function($){

    //открыть модальное окно
    $('#cpate-order').on('click', function(event){

        $('.cpate-modal').addClass('is-visible');   
    });

    //закрыть модальное окно
    $('.cpate-modal').on('click', function(event){
        if( $(event.target).is($('.cpate-modal')) || $(event.target).is('.cd-close-form') ) {
            $('.cpate-modal').removeClass('is-visible');
        }   
    });

    //закрыть модальное окно нажатии клавиши Esc 
    $(document).keyup(function(event){
        if(event.which=='27'){
            $('.cpate-modal').removeClass('is-visible');
        }
    });

    //спрятать ошибки при нажатии на input 
    $(".cpate-form input[type=text]").keyup(function(event){
        $(".has-border").removeClass('has-error');
        $(".cd-error-message").removeClass('is-visible');
    });

    //вывод ошибки, если незаполнено поле 
    $('.cpate-form input[type="submit"]').on('click', function(event){
        event.preventDefault();

        var name  = $(".cpate-form input[name=name]").val();
        var phone = $(".cpate-form input[name=phone]").val();

        if (name.length == 0) {
            $('.cpate-form').find('input[name=name]').next('span').toggleClass('is-visible').fadeIn('slow');
            return false; 
        }
        if (phone.length == 0) {
            $('.cpate-modal').find('input[name=phone]').toggleClass('has-error').next('span').toggleClass('is-visible').fadeIn('slow');
            return false;
        } 

        $('.cpate-form').submit();      
    });
});
    var modal = '<div class="cpate-modal">'+
                    '<div class="cpate-modal-container">'+
                        '<p class="cpate-modal-title">Заполните форму и наш оператор свяжется с Вами в ближайшее время</p>'+
                        '<form class="cpate-form" action="../../_/thanks.php" method="post">'+
                            '<p class="fieldset">'+
                                '<label class="image-replace cd-username" for="name">Ваше имя</label>'+
                                '<input class="full-width has-padding has-border" id="name" name="name" type="text" placeholder="Имя">'+
                                '<span class="cd-error-message">Укажите Ваше имя!</span>'+
                            '</p>'+
                            '<p class="fieldset">'+
                                '<label class="image-replace cd-phone" for="phone">Ваш телефон</label>'+
                                '<input class="full-width has-padding has-border" id="phone" name="phone" type="text" placeholder="Телефон">'+
                                '<span class="cd-error-message">Укажите номер телефона!</span>'+
                            '</p>'+
                            '<p class="fieldset">'+
                                '<input class="full-width has-padding" type="submit" value="ЗАКАЗАТЬ">'+
                            '</p>'+
                        '</form><a href="#0" class="cd-close-form">Close</a>'+
                    '</div>'+
                '</div>';


    $(modal).appendTo($(document.body)); 


function cpateForm(){

    if ($(window).width() <= '420')
    {
        $('html, body').animate({scrollTop: 0},0);
    } 

    $('.cpate-modal').addClass('is-visible');
}

/*
function validate_form()
{
    var data = {};
    data.name = $(this).find('[name="name"]')[0];
    data.phone = $(this).find('[name="phone"]')[0];
    if (data.name != null && data.name.value.length < 3)
    {
        alert('Пожалуйста укажите Ваше имя!');
        return false;
    }

    if (data.phone != null && data.phone.value.length < 6) {
        
        alert('Пожалуйста укажите правильно Ваш номер телефона!\n');
        return false;
    }

}

(function ($) {

    $('form[action^="../../_/thanks.php"]').off('submit').submit(validate_form);

})(jQuery);        

*/

var cityList = ['Абакан', 'Абан', 'Абатский', 'Абрамовский маяк', 'Агата', 'Агаякан', 'Агзу', 'Агинское', 'Агинское', 'Айхал', 'Акша', 'Акьяр', 'Алапаевск', 'Алатырь', 'Алдан', 'Алейская', 'Александров', 'Александров-Гай', 'Александровск-Сахалинский', 'Александровский Шлюз', 'Александровский з-д', 'Александровское', 'Александровское', 'Аллах-Юнь', 'Алтай', 'Алыгджер', 'Амазар', 'Амга', 'Амдерма', 'Анабар', 'Анадырь', 'Анапа', 'Андрея', 'Андрюшкино', 'Анна', 'Антипаюта', 'Анучино', 'Апатиты', 'Апука', 'Аргаяш', 'Арзгир', 'Арка', 'Армавир', 'Армань', 'Арсеньев', 'Артезиан', 'Артем', 'Артемовск', 'Артемовский', 'Архангельск', 'Архара', 'Аршан', 'Аскиз', 'Астраханка', 'Астрахань', 'Ахты', 'Ачинск', 'Аян', 'Бабаево', 'Бабушкин', 'Баган', 'Багдарин', 'Баево', 'Байдуков', 'Байкальск', 'Байкит', 'Бакалы', 'Бактор', 'Бакчар', 'Балаганск', 'Баладек', 'Балаково', 'Балахта', 'Балашов', 'Балей', 'Балтийск', 'Балыгычан', 'Барабаш', 'Барабинск', 'Бараниха', 'Баргузин', 'Баргузинский Заповедник    \t', 'Барзас', 'Барнаул', 'Батагай', 'Батагай-Алыта', 'Батамай', 'Батомга', 'Батурино', 'Бахта', 'Баяндай', 'Беклемишево', 'Белая Гора', 'Белая глина', 'Белгород', 'Белово', 'Белогорка', 'Белогорск', 'Белозерск', 'Белый Яр', 'Белый', 'Беля', 'Бердигестях', 'Березники', 'Березняки', 'Березовка', 'Березово', 'Березово', 'Бестяхск', 'Бея', 'Бийск', 'Бийск-Зональная', 'Бикин', 'Билибино', 'Биробиджан', 'Бирск', 'Бисер', 'Бисер', 'Бисерть', 'Бичевая', 'Бичура', 'Благовещенск', 'Благодарный', 'Богородское', 'Боготол', 'Богучаны', 'Бодайбо', 'Боковская', 'Бологое', 'Болотное', 'Болхов', 'Большая Глушица', 'Большая Лепринда', 'Большая Мурта', 'Большерецк', 'Большеречье', 'Большие Кайбицы', 'Большие Уки', 'Большое Голоустное', 'Большой Он', 'Большой Порог', 'Большой Улуй', 'Бомнак', 'Бор', 'Борзя', 'Борисоглебск', 'Борогонцы', 'Бохан', 'Бохапча', 'Братолюбовка', 'Братск', 'Братск', 'Бреды', 'Бродокалмак', 'Брохово', 'Брянск', 'Бугрино', 'Бугульма', 'Бугуруслан', 'Буденновск', 'Бузулук', 'Буй', 'Буйнакск', 'Букукун', 'Бурукан', 'Бухта Амбарчик', 'Бухта Провидения', 'Буяга', 'Быково', 'Бысса', 'Вайда Губа', 'Вал', 'Валькаркай', 'Ванавара', 'Ванжиль-Кынак', 'Ваховск', 'Великие Луки', 'Великий Новгород', 'Великий Устюг', 'Вельмо', 'Вельск', 'Венгерово', 'Верещагино', 'Верещагино', 'Верхневилюйск', 'Верхнее Дуброво', 'Верхнее Пенжино', 'Верхнеимбатск', 'Верхний Амыл', 'Верхний Баскунчак', 'Верхний Уфалей', 'Верхняя Гутара', 'Верхняя Тойма', 'Верхняя Томь', 'Верховье Умри', 'Верховье р. Лотты', 'Верховье', 'Верхотурье', 'Верхоянск', 'Верхоянский Перевоз', 'Веселая Горка', 'Весляна', 'Ветлуга', 'Викулово', 'Вилюйск', 'Винницы', 'Висим', 'Витим', 'Владивосток', 'Владикавказ', 'Владимир', 'Внуково', 'Водопадная', 'Вожега', 'Вознесенье', 'Волгоград', 'Волжская ГМО', 'Волово', 'Вологда', 'Волоколамск', 'Волосово', 'Волчиха', 'Воньеган', 'Воркута', 'Ворогово', 'Воронеж', 'Воскресенское', 'Восток', 'Восточная', 'Воткинск', 'Вохма', 'Вуктыл', 'Выборг', 'Выкса', 'Вытегра', 'Вышний Волочек', 'Вяземская', 'Вязники', 'Вязьма', 'Вятские Поляны', 'ГМО им. Е.К.Федорова', 'ГМО им. Е.Т.Кренкеля', 'Гагарин', 'Гайны', 'Гамов', 'Гари', 'Гвасюги', 'Гдов', 'Геленджик', 'Георгиевка', 'Гигант', 'Гижига', 'Глазов', 'Глубинное', 'Голышманово', 'Горекацан', 'Горин', 'Горно-Алтайск', 'Городище', 'Городовиковск', 'Горячий Ключ', 'Горячинск', 'Готня', 'Гоуджекит', 'Гридино', 'Грозный', 'Губа Дроздовка', 'Губаха', 'Гуга', 'Гусь-Хрустальный', 'Гыдоямо', 'Далматово', 'Дальнереченск', 'Даниловка', 'Дарасун', 'Даровское', 'Двинский Березняк', 'Дебессы', 'Делянкир', 'Демьянское', 'Депутатский', 'Дербент', 'Джалинда', 'Джалинда', 'Джана', 'Джарджан', 'Джека Лондона', 'Джикимда', 'Джубга', 'Дзержинское', 'Дивное', 'Дмитров', 'Дмитровск-Орловский', 'Дно', 'Довольное', 'Должанская', 'Долиновка', 'Домодедово', 'Доно', 'Дорожный', 'Дуван', 'Дудинка', 'Егорьевск', 'Ейск', 'Екатеринбург', 'Екатерино-Никольское', 'Елабуга', 'Елань', 'Елатьма', 'Елец', 'Емеск', 'Енисейск', 'Ербогачен', 'Ермаковское', 'Ерофей Павлович', 'Ершов', 'Ессей', 'Ефимовская', 'Ефремов', 'Железнодорожный', 'Жердевка', 'Жигалово', 'Жиганск', 'Жижгин', 'Жиздра', 'Жуковка', 'Забайкальск', 'Завитая', 'Залари', 'Замакта', 'Заметчино', 'Зареченск', 'Заринск', 'Зашеек', 'Заярск', 'Звериноголовское', 'Здвинск', 'Зерноград', 'Зея', 'Зилаир', 'Зима', 'Зимовники', 'Златоуст', 'Змеиногорск', 'Золотой', 'Зырянка', 'Иваново', 'Ивдель', 'Игарка', 'Игнашино', 'Игрим', 'Идринское', 'Ижевск', 'Ижма', 'Ика', 'Илирней', 'Иловля', 'Ильинский', 'Им. М.В. Попова', 'Им. Полины Осипенко', 'Им.Е.К.Федорова', 'Инга', 'Индига', 'Индигирская', 'Инза', 'Инсар', 'Иоли', 'Ирбейское', 'Ирбит-Фомино', 'Иркутск', 'Исилькуль', 'Исить', 'Искитим', 'Ича', 'Ишим', 'Иэма', 'Йошкар-Ола', 'Кабанск', 'Кавалерово', 'Казань', 'Казачинское', 'Казачинское', 'Казым', 'Казыр', 'Кайластуй', 'Калакан', 'Калач', 'Калачинск', 'Калгачиха', 'Калевала', 'Калининград', 'Калининск', 'Калуга', 'Каменск-Уральский', 'Каменск-Шахтинский', 'Каменское', 'Камень-на-Оби', 'Камышлов', 'Канадей', 'Канаш', 'Кандалакша', 'Каневка', 'Каневская', 'Канин Нос', 'Канку', 'Канск', 'Кантегир', 'Каньон', 'Карасук', 'Каратузское', 'Карафтит', 'Карачев', 'Каргасок', 'Каргат', 'Каргополь', 'Карпогоры', 'Карталы', 'Карымская', 'Катав-Ивановск', 'Катанда', 'Катугино', 'Кача', 'Качуг', 'Кашин', 'Кашира', 'Кашкаранцы', 'Кегали', 'Кедва-Вом', 'Кедон', 'Келлог', 'Кемерово', 'Кемчуг', 'Кемь-Порт', 'Кербо', 'Кизильское', 'Кизляр', 'Килеер', 'Кильмезь', 'Кингисепп', 'Кинешма', 'Киренск', 'Кириши', 'Киров', 'Кировский', 'Кирс', 'Кирсанов', 'Киселевск', 'Клин', 'Клухорский перевал', 'Ключи', 'Ключи', 'Ковда', 'Ковдор', 'Когалым', 'Кожевниково', 'Козыревск', 'Козьмодемьянск', 'Койнас', 'Колба', 'Колгуев Северный', 'Колежма', 'Колмъявр', 'Кологрив', 'Коломна', 'Колпашево', 'Колывань', 'Кольцово', 'Комака', 'Коммунар', 'Комрво', 'Комсомольск-на-Амуре', 'Комсомольский', 'Кондома', 'Кондопога', 'Конево', 'Константиновск', 'Конь-Колодезь', 'Коркодон', 'Корсаков', 'Корф', 'Коса', 'Кослан', 'Кострома', 'Котельнич', 'Коткино', 'Котлас', 'Кочево', 'Коченёво', 'Кочки', 'Кочубей', 'Кош-Агач', 'Крапивино', 'Красная Гора', 'Красногвардейское', 'Краснодар', 'Красное поселение', 'Красное', 'Красноозерск', 'Красноселькупск', 'Краснослободск', 'Красноуфимск', 'Краснощеково', 'Краснощелье', 'Красноярск', 'Красные Баки', 'Красный Кут', 'Красный Холм', 'Красный Чикой', 'Красный Яр', 'Красный Яр', 'Крест-Хальджай', 'Кресты Таймырские', 'Крестях', 'Крещенка', 'Кроноки', 'Кропоткин', 'Крымск', 'Ксеньевская', 'Кубанская', 'Куганаволок', 'Кудымкар', 'Кузедеево', 'Кузьмовка', 'Култук', 'Кулу', 'Кумены', 'Куминская', 'Кунгур', 'Купино', 'Кур', 'Курагино', 'Курган', 'Курейка', 'Курильск', 'Курск', 'Куртамыш', 'Курумкан', 'Курун-Урях', 'Кушва', 'Кущевская', 'Кызыл', 'Кызыл-Озек', 'Кыкер', 'Кыра', 'Кырен', 'Кытлым', 'Кыштовка', 'Кюсюр', 'Кяхта', 'Лабазная', 'Лаган', 'Лазо', 'Лаишево', 'Лальск', 'Ларьяк', 'Лебяжье', 'Лебяжье', 'Лев Толстой', 'Ленинское', 'Ленск', 'Лермонтовка', 'Лесозаводск', 'Леуши', 'Лешуконское', 'Ливны', 'Липецк', 'Лиски', 'Литке', 'Ловозеро', 'Лодейное поле', 'Локшак', 'Лопча', 'Лосиноборское', 'Лукоянов', 'Лысково', 'Лысьва', 'Льгов', 'М. Лопатка', 'Магадан', 'Магдагачи', 'Магнитогорск', 'Мадаун', 'Мазаново', 'Майкоп', 'Майск', 'Максатиха', 'Макушино', 'Малая кема', 'Малиново', 'Малоярославец', 'Малые Дербеты', 'Малые Кармакулы', 'Мама', 'Мангут', 'Маргаритово', 'Мариинск', 'Марково', 'Марресаля', 'Маслянино', 'Матвеев Курган', 'Махачкала', 'Мача', 'Маячный', 'Медвежьегорск', 'Междуреченск', 'Мезень', 'Мелеуз', 'Мельничное', 'Менза', 'Миасс', 'Миллерово', 'Мильково', 'Минеральные воды', 'Минусинск', 'Мирный', 'Михайловск', 'Мичуринск', 'Могзон', 'Могоча', 'Можайск', 'Можга', 'Моздок', 'Молодежная', 'Молчаново', 'Монды', 'Монерон', 'Мончегорск', 'Мопау', 'Моржовая', 'Моржовец', 'Морки', 'Морозовск', 'Моршанск', 'Мосальск', 'Мосеево', 'Москва', 'Мотыгино', 'Мошково', 'Мугур-Аксы', 'Мудьюг', 'Мужи', 'Мурманск', 'Мурмаши', 'Муслюмово', 'Мутный Материк', 'Мухоршибирь', 'Мценск', 'Мыс Алевина', 'Мыс Африка', 'Мыс Белый Нос', 'Мыс Биллингса', 'Мыс Братьев', 'Мыс Кигилях', 'Мыс Костантиновский', 'Мыс Микулкин', 'Мыс Озерный', 'Мыс Салаурова', 'Мыс Стерлигова', 'Мыс Терпения', 'Мыс Уэлен', 'Мыс Шмидта', 'Нагорный', 'Нагорское', 'Надым', 'Назарово', 'Назимово', 'Называевск', 'Нальчик', 'Намцы', 'Напас', 'Наро-Фоминск', 'Нарьян-Мар', 'Находка', 'Начики', 'Невельск', 'Невинномыск', 'Невьянск', 'Нелькан', 'Немчиновка', 'Ненастная', 'Неожиданный', 'Нера', 'Нерой', 'Нерчинск', 'Нерчинский Завод', 'Несь', 'Нефтеюганск', 'Ниванкуль', 'Нижне-Тамбовское', 'Нижне-Усинское', 'Нижнеангарск', 'Нижневартовск', 'Нижнесортымск', 'Нижнеудинск', 'Нижнеянск', 'Нижний Новгород', 'Нижний Тагил', 'Нижний Цасучей', 'Нижний Чир', 'Нижняя Пеша', 'Никель', 'Николаевск-на-Амуре', 'Николаевская', 'Николо-Полома', 'Никольск', 'Никольское', 'Ничатка', 'Новая Ладога', 'Новиково', 'Ново-Александровск', 'Ново-Иерусалим', 'Ново-Касторное', 'Новобирилюссы', 'Новокузнецк', 'Новолазаревская', 'Новороссийск', 'Новоселенгинск', 'Новосибирск', 'Новочунка', 'Новый Васюган', 'Новый Оскол', 'Новый Порт', 'Новый Торьял', 'Новый Уренгой', 'Ноглики', 'Ножовка', 'Нолинск', 'Нора', 'Норильск', 'Норск', 'Ноябрьск', 'Ныврово', 'Ныда', 'Нюрба', 'Нюя', 'Нязепетровск', 'Няксимволь', 'Няндома', 'Обловка', 'Облучье', 'Обоянь', 'Обская ГМО', 'Обьячево', 'Огурцово', 'Одесское', 'Озерки', 'Озерная', 'Озеро Таймыр', 'Озинки', 'Оймякон', 'Октябрьская', 'Октябрьское', 'Окунев Нос', 'Ола', 'Олекминск', 'Оленек', 'Оленья Речка', 'Оловянная', 'Олонец', 'Ольга', 'Омолон', 'Омск', 'Омсукчан', 'Онгудай', 'Онега', 'Опарино', 'Опочка', 'Орджоникидзевская', 'Ордынское', 'Орел', 'Оренбург', 'Орлик', 'Орлинга', 'Оса', 'Оссора', 'Осташков', 'Остров  Айон', 'Остров  Валаам', 'Остров  Вилькицкого', 'Остров  Голомянный', 'Остров  Русский', 'Остров  Спафарьева', 'Остров  Харлов', 'Остров Большой Ушканий', 'Остров Большой Шантар', 'Остров Визе', 'Остров Врангеля', 'Остров Диксон', 'Остров Котельный', 'Остров Преображения', 'Остров Тюлений', 'Острова Дунай', 'Острова Известий', 'Острова Челно-Вершины', 'Оха', 'Оханск', 'Охотничий', 'Охотск', 'Охотский Перевоз', 'Павелец', 'Павлово', 'Павловск', 'Павловский Посад', 'Павловское', 'Павлоградка', 'Паданы', 'Падун', 'Палана', 'Палатка', 'Памятная', 'Пангоды', 'Парабель', 'Партизанск', 'Пачелма', 'Певек', 'Пенза', 'Первомайское', 'Первомайское', 'Перелюб', 'Переславль-Залесский', 'Пермь', 'Петровск', 'Петровский Завод', 'Петрозаводск', 'Петрокрепость', 'Петропавловка', 'Петропавловск-Камчатский', 'Петропавловский Маяк', 'Петрунь', 'Петухово', 'Петушки', 'Печора', 'Пильво', 'Пинега', 'Пионерский', 'Пировское', 'Питляр', 'Погиби', 'Пограничное', 'Пограничный', 'Подгорное', 'Покровка', 'Покровская', 'Полигус', 'Половинное', 'Полтавка', 'Полтавка', 'Полуй', 'Полярный', 'Полярный', 'Помоздино', 'Понил', 'Поныри', 'Поронайск', 'Посевная', 'Поспелиха', 'Посьет', 'Пошехонье-Володарск', 'Поярково', 'Преображение', 'Приаргунск', 'Приволжск', 'Приморско-Ахтарск', 'Пролив Санникова', 'Промышленная', 'Прохладная', 'Прохоркино', 'Псков', 'Пугачев', 'Пудино', 'Пудож', 'Пустошь', 'Пушкинские Горы', 'Пущино', 'Пыщуг', 'Пялица', 'Пятигорск', 'Ра-Из', 'Радужный', 'Разнаволок', 'Реболы', 'Ребриха', 'Ревда', 'Ремонтное', 'Ржев', 'Родино', 'Родниковая', 'Романовка', 'Рославль', 'Ростов', 'Ростов-на-Дону', 'Рощино', 'Рощиной', 'Ртищево', 'Рубцовск', 'Рудная Пристань', 'Русская Поляна', 'Рыбинск', 'Рыльск', 'Ряжск', 'Рязань', 'Салемал', 'Салехард', 'Самара', 'Самарка', 'Санага', 'Сангары', 'Санкт-Петербург', 'Саныяхтат', 'Саранпауль', 'Саранск', 'Сарапул', 'Саратов', 'Саргатское', 'Саров', 'Сарыг-Сеп', 'Саскылах', 'Сасово', 'Светлоград', 'Светлолобово', 'Светлый', 'Свиягино', 'Свободный', 'Святой Нос', 'Северное', 'Северо-Енисейск', 'Северо-Курильск', 'Северодвинск', 'Североуральск', 'Сегежа', 'Сеген-Кюель', 'Сегжема', 'Сеймчан', 'Сектагли', 'Селты', 'Семячик', 'Сенгейский Шар', 'Сергач', 'Сергеевка', 'Сергокала', 'Серов', 'Серов', 'Серпухов', 'Сеяха', 'Сидоровск', 'Симушир', 'Сковородино', 'Славгород', 'Славянск-на-Кубани', 'Сладково', 'Слаутное', 'Смидович', 'Смоленск', 'Советск', 'Советская Гавань', 'Советская Речка', 'Совхоз Эльген', 'Совхоз им.Ленина', 'Сого-Хая', 'Солекуль', 'Солнечная', 'Соловьевск', 'Солонешное', 'Сопочная карга', 'Сортавала', 'Сосновка', 'Сосновка', 'Сосново', 'Сосново-Озерское', 'Сосуново', 'Сосьва', 'Софийский Прииск', 'Сочи (Адлер)', 'Спас-Деменск', 'Среднеколымск', 'Средний Васюган', 'Средний Калар', 'Средникан', 'Средняя Олекма', 'Сретенск', 'Ставрополь', 'Старица', 'Старица', 'Старый Оскол', 'Степановка', 'Стерлитамак', 'Столб', 'Стрелка', 'Сузун', 'Сукпай', 'Сунтар', 'Суон-Тит', 'Суоярви', 'Сура', 'Сургут', 'Сусуман', 'Сухана', 'Сухиничи', 'Сухобузимское', 'Сызрань', 'Сыктывкар', 'Сым', 'Сысерть', 'Сытомино', 'Таборы', 'Тавда', 'Таганрог', 'Таежная', 'Тазовский', 'Тайга', 'Тайгонос', 'Таймылыр', 'Тайшет', 'Таксимо', 'Талая', 'Талон', 'Тальменка', 'Тамбей', 'Тамбов', 'Тангуй', 'Танхой', 'Танюрер', 'Тара', 'Тарко-Сале', 'Тасеево', 'Тасса', 'Татарск', 'Таурово', 'Таштып', 'Тверь', 'Теви', 'Тевриз', 'Тегульдет', 'Тегультя', 'Телемба', 'Темников', 'Теплый Ключ', 'Тереховка', 'Териберка', 'Терней', 'Тетюши', 'Тивяку', 'Тигиль', 'Тикси', 'Тилишма', 'Тим', 'Тимирязевский', 'Тисуль', 'Тихвин', 'Тихорецк', 'Тобольск', 'Тогул', 'Тогучин', 'Токо', 'Толмачево', 'Толька', 'Тольятти', 'Томмот', 'Томпа', 'Томпо', 'Томск', 'Тонгулах', 'Тоора-Хем', 'Топки', 'Торбеево', 'Торжок', 'Торопец', 'Тотьма', 'Третьяково', 'Троицк', 'Троицко-Печорское', 'Троицкое', 'Троицкое', 'Трубчевск', 'Туапсе', 'Тугулым', 'Тула', 'Тулун', 'Тума', 'Туманная', 'Тумнин', 'Тунгокочен', 'Тунка', 'Туой-Хая', 'Тупик', 'Тура', 'Турий Рог', 'Туринск', 'Турочак', 'Туруханск', 'Турчасово', 'Тутончаны', 'Тында', 'Тырка', 'Тюкалинск', 'Тюмень', 'Тюмети', 'Тюхтет', 'Тяжин', 'Тяня', 'Уакит', 'Убинское', 'Угино', 'Углегорск', 'Угловское', 'Угут', 'Удское', 'Уега', 'Ужаниха', 'Ужур', 'Улан-Удэ', 'Улеты', 'Улья', 'Ульяновск', 'Умба', 'Унеча', 'Уни', 'Ура-Губа', 'Урми', 'Уруп', 'Урюпинск', 'Усолье-Сибирское', 'Усть-Антосе', 'Усть-Баргузин', 'Усть-Воямполка', 'Усть-Заза', 'Усть-Илимск', 'Усть-Ишим', 'Усть-Кабырза', 'Усть-Камо', 'Усть-Камчатск', 'Усть-Кан', 'Усть-Кара', 'Усть-Кара', 'Усть-Каренга', 'Усть-Кокса', 'Усть-Кулом', 'Усть-Кут', 'Усть-Лабинск', 'Усть-Мая', 'Усть-Миль', 'Усть-Мома', 'Усть-Нюкжа', 'Усть-Озерное', 'Усть-Олой', 'Усть-Омчуг', 'Усть-Ордынский', 'Усть-Тарка', 'Усть-Уда', 'Усть-Умальта', 'Усть-Уса', 'Усть-Уса', 'Усть-Хайрюзово', 'Усть-Цильма', 'Усть-Чаркы', 'Усть-Юдома', 'Усугли', 'Уфа', 'Ухта', 'Учами', 'Учур', 'Ушки', 'Уяр', 'Фролово', 'Хабардино', 'Хабаровск', 'Хабары', 'Хакасская', 'Хамар-Дабан', 'Ханты-Мансийск', 'Харабали', 'Хасавюрт', 'Хатанга', 'Хатырык-Хомо', 'Хвалынск', 'Хейджан', 'Хилок', 'Хову-Аксы', 'Ходовариха', 'Холмогоры', 'Холмск', 'Хорей-Вер', 'Хоринск', 'Хороль', 'Хоседа-Хард', 'Хуларин', 'Хулугли', 'Цакир', 'Целина', 'Целинное', 'Целинное', 'Центральный рудник', 'Цимлянск', 'Циммермановка', 'Цып-Наволок', 'Чаваньга', 'Чадан', 'Чаингда', 'Чайковский', 'Чаны', 'Чара', 'Чарышское', 'Чаун', 'Чебоксары', 'Чекунда', 'Челябинск', 'Чемал', 'Чемдальск', 'Чемурнаут', 'Червянка', 'Чердынь', 'Черемушки', 'Черемхово', 'Черемхово', 'Череповец', 'Черкесск', 'Черлак', 'Чермоз', 'Чернушка', 'Чернышевский', 'Черняево', 'Черняховск', 'Черский', 'Чертково', 'Черусти', 'Чистоозерное', 'Чистополь', 'Чита', 'Чокурдах', 'Чугуевка', 'Чулпаново', 'Чулым', 'Чульман', 'Чумикан', 'Чумпурук', 'Чурапча', 'Чюльбю', 'Шабалино', 'Шадринск', 'Шаим', 'Шалинское', 'Шамары', 'Шангалы', 'Шарыпово', 'Шарья', 'Шатрово', 'Шахты', 'Шахунья', 'Шебалино', 'Шевли', 'Шелаболиха', 'Шелагонцы', 'Шелехова', 'Шелопугино', 'Шенкурск', 'Шербакуль', 'Шереметьево', 'Шилка', 'Шимановск', 'Шира', 'Шойна', 'Шумиха', 'Шумиха', 'Щетинкино', 'Ыныкчан', 'Ытык-Кель', 'Эгвекинот', 'Эйк', 'Экимчан', 'Элиста', 'Эльтон', 'Энгозеро', 'Энкан', 'Эньмувеем', 'Эрзин', 'Эссо', 'Югоренок', 'Южно-Курильск', 'Южно-Сахалинск', 'Южно-Сухокумск', 'Южноуральск', 'Юильск', 'Юмурчен', 'Юрга', 'Юрты', 'Юрьев-Польский', 'Юрьевец', 'Юста', 'Юшкозеро', 'Ягодное', 'Яйлю', 'Яковлевка', 'Якутск', 'Якша', 'Ялуторовск', 'Ямкун', 'Янаул', 'Янискоски', 'Янов Стан', 'Янск', 'Яранск', 'Яренск', 'Ярольин', 'Ярославль', 'Ярцево', 'Яшкуль', 'Яя'];


var peoples = [{
    "fio": "Иноземцева Надежда",
    "image": "../img/women1.jpg",
    "sex": 0
}, {
    "fio": "Тотенкова Регина",
    "image": "../img/women2.jpg",
    "sex": 0
}, {
    "fio": "Курдина Эмилия",
    "image": "../img/women3.jpg",
    "sex": 0
}, {
    "fio": "Стаина Анна",
    "image": "../img/women4.jpg",
    "sex": 0
}, {
    "fio": "Чуличкова Анастасия",
    "image": "../img/women5.jpg",
    "sex": 0
}, {
    "fio": "Шеркова Евгения",
    "image": "../img/women6.jpg",
    "sex": 0
}, {
    "fio": "Андрюхина Нина",
    "image": "../img/women7.jpg",
    "sex": 0
}, {
    "fio": "Катериночкина Анфиса",
    "image": "../img/women8.jpg",
    "sex": 0
}, {
    "fio": "Головина Анна",
    "image": "../img/women9.jpg",
    "sex": 0
}, {
    "fio": "Чупрова Екатерина",
    "image": "../img/women10.jpg",
    "sex": 0
}, {
    "fio": "Холопова Виктория",
    "image": "../img/women11.jpg",
    "sex": 0
}, {
    "fio": "Крупина Мария",
    "image": "../img/women12.jpg",
    "sex": 0
}, {
    "fio": "Полевщикова Кристина",
    "image": "../img/women13.jpg",
    "sex": 0
}, {
    "fio": "Пьянкова Диана",
    "image": "../img/women14.jpg",
    "sex": 0
}, {
    "fio": "Буланова Яна",
    "image": "../img/women15.jpg",
    "sex": 0
}, {
    "fio": "Цейдлерина Мария",
    "image": "../img/women16.jpg",
    "sex": 0
}, {
    "fio": "Щеголева Светлана",
    "image": "../img/women17.jpg",
    "sex": 0
}, {
    "fio": "Янкелевич Алина",
    "image": "../img/women18.jpg",
    "sex": 0
}, {
    "fio": "Якушевич Наталья",
    "image": "../img/women19.jpg",
    "sex": 0
}, {
    "fio": "Фомичева Диана",
    "image": "../img/women20.jpg",
    "sex": 0
}, {
    "fio": "Пережогина Виктория",
    "image": "../img/women21.jpg",
    "sex": 0
}, {
    "fio": "Ячменькова Василиса",
    "image": "../img/women22.jpg",
    "sex": 0
}, {
    "fio": "Рябова Дарья",
    "image": "../img/women23.jpg",
    "sex": 0
}, {
    "fio": "Домышева Юлия",
    "image": "../img/women24.jpg",
    "sex": 0
}, {
    "fio": "Милова Татьяна",
    "image": "../img/women25.jpg",
    "sex": 0
}, {
    "fio": "Шипицына Анна",
    "image": "../img/women26.jpg",
    "sex": 0
}, {
    "fio": "Протасова Евгения",
    "image": "../img/women27.jpg",
    "sex": 0
}, {
    "fio": "Молодыха Алиса",
    "image": "../img/women28.jpg",
    "sex": 0
}, {
    "fio": "Коржева Ксения",
    "image": "../img/women29.jpg",
    "sex": 0
}, {
    "fio": "Кузнецова Вероника",
    "image": "../img/women30.jpg",
    "sex": 0
}, {
    "fio": "Сукина Алиса",
    "image": "../img/women31.jpg",
    "sex": 0
}, {
    "fio": "Перова ?Агата",
    "image": "../img/women32.jpg",
    "sex": 0
}, {
    "fio": "Коржакова Ольга",
    "image": "../img/women33.jpg",
    "sex": 0
}, {
    "fio": "Ёжина Вероника",
    "image": "../img/women34.jpg",
    "sex": 0
}, {
    "fio": "Абрамович Валентина",
    "image": "../img/women35.jpg",
    "sex": 0
}, {
    "fio": "Крылова Наталья",
    "image": "../img/women36.jpg",
    "sex": 0
}, {
    "fio": "Проскуркина Александра",
    "image": "../img/women37.jpg",
    "sex": 0
}, {
    "fio": "Терехова Юлия",
    "image": "../img/women38.jpg",
    "sex": 0
}, {
    "fio": "Труфанова Варвара",
    "image": "../img/women39.jpg",
    "sex": 0
}, {
    "fio": "Батурина Марина",
    "image": "../img/women40.jpg",
    "sex": 0
}, {
    "fio": "Васнецова Нина",
    "image": "../img/women41.jpg",
    "sex": 0
}, {
    "fio": "Перевалова Надежда",
    "image": "../img/women42.jpg",
    "sex": 0
}, {
    "fio": "Рошета Любовь",
    "image": "../img/women43.jpg",
    "sex": 0
}, {
    "fio": "Мосякова Татьяна",
    "image": "../img/women44.jpg",
    "sex": 0
}, {
    "fio": "Носова Анастасия",
    "image": "../img/women45.jpg",
    "sex": 0
}, {
    "fio": "Типалова Юнона",
    "image": "../img/women46.jpg",
    "sex": 0
}, {
    "fio": "Колесникова Инесса",
    "image": "../img/women47.jpg",
    "sex": 0
}, {
    "fio": "Якуничева Анна",
    "image": "../img/women48.jpg",
    "sex": 0
}, {
    "fio": "Левина Евгения",
    "image": "../img/women49.jpg",
    "sex": 0
}, {
    "fio": "Агафонова Виктория",
    "image": "../img/women50.jpg",
    "sex": 0
}, {
    "fio": "Дуркина Антонина",
    "image": "../img/women51.jpg",
    "sex": 0
}, {
    "fio": "Игошина Мария",
    "image": "../img/women52.jpg",
    "sex": 0
}, {
    "fio": "Званцова Светлана",
    "image": "../img/women53.jpg",
    "sex": 0
}, {
    "fio": "Хлопонина Елена",
    "image": "../img/women54.jpg",
    "sex": 0
}, {
    "fio": "Суботина Изабелла",
    "image": "../img/women55.jpg",
    "sex": 0
}, {
    "fio": "Дроздова Марфа",
    "image": "../img/women56.jpg",
    "sex": 0
}, {
    "fio": "Милютина Изабелла",
    "image": "../img/women57.jpg",
    "sex": 0
}, {
    "fio": "Гнусарева Ангелина",
    "image": "../img/women58.jpg",
    "sex": 0
}, {
    "fio": "Домаш Вячеслав",
    "image": "../img/men1.jpg",
    "sex": 1
}, {
    "fio": "Лагутов Руслан",
    "image": "../img/men2.jpg",
    "sex": 1
}, {
    "fio": "Степанков Радислав",
    "image": "../img/men3.jpg",
    "sex": 1
}, {
    "fio": "Перешивкин Ростислав",
    "image": "../img/men4.jpg",
    "sex": 1
}, {
    "fio": "Кобзев Платон",
    "image": "../img/men5.jpg",
    "sex": 1
}, {
    "fio": "Кабанов Игнатий",
    "image": "../img/men6.jpg",
    "sex": 1
}, {
    "fio": "Чепурин Николай",
    "image": "../img/men7.jpg",
    "sex": 1
}, {
    "fio": "Крымов Изяслав",
    "image": "../img/men8.jpg",
    "sex": 1
}, {
    "fio": "Собчак Евгений",
    "image": "../img/men9.jpg",
    "sex": 1
}, {
    "fio": "Былинкин Максим",
    "image": "../img/men10.jpg",
    "sex": 1
}, {
    "fio": "Архипов Сергей",
    "image": "../img/men11.jpg",
    "sex": 1
}, {
    "fio": "Донцов Самсон",
    "image": "../img/men12.jpg",
    "sex": 1
}, {
    "fio": "Стаин Владилен",
    "image": "../img/men13.jpg",
    "sex": 1
}, {
    "fio": "Лызлов Владислав",
    "image": "../img/men14.jpg",
    "sex": 1
}, {
    "fio": "Ягужинский Аристарх",
    "image": "../img/men15.jpg",
    "sex": 1
}, {
    "fio": "Фризов Владимир",
    "image": "../img/men16.jpg",
    "sex": 1
}, {
    "fio": "Крупнов Дмитрий",
    "image": "../img/men17.jpg",
    "sex": 1
}, {
    "fio": "Ябловский Вадим",
    "image": "../img/men18.jpg",
    "sex": 1
}, {
    "fio": "Гусенков Самсон",
    "image": "../img/men19.jpg",
    "sex": 1
}, {
    "fio": "Панфёров Семён",
    "image": "../img/men20.jpg",
    "sex": 1
}, {
    "fio": "Ясинский Павел",
    "image": "../img/men21.jpg",
    "sex": 1
}, {
    "fio": "Тетерев Глеб",
    "image": "../img/men22.jpg",
    "sex": 1
}, {
    "fio": "Шлыков Николай",
    "image": "../img/men23.jpg",
    "sex": 1
}, {
    "fio": "Козлов Илья",
    "image": "../img/men24.jpg",
    "sex": 1
}, {
    "fio": "Амалиев Максим",
    "image": "../img/men25.jpg",
    "sex": 1
}, {
    "fio": "Паулкин Ефим",
    "image": "../img/men26.jpg",
    "sex": 1
}, {
    "fio": "Колганов Герман",
    "image": "../img/men27.jpg",
    "sex": 1
}, {
    "fio": "Саламатов Николай",
    "image": "../img/men28.jpg",
    "sex": 1
}, {
    "fio": "Сподарев Степан",
    "image": "../img/men29.jpg",
    "sex": 1
}, {
    "fio": "Бочкарёв Владимир",
    "image": "../img/men30.jpg",
    "sex": 1
}, {
    "fio": "Опекунов Вячеслав",
    "image": "../img/men31.jpg",
    "sex": 1
}, {
    "fio": "Телицын Тимофей",
    "image": "../img/men32.jpg",
    "sex": 1
}, {
    "fio": "Ямлиханов Андрей",
    "image": "../img/men33.jpg",
    "sex": 1
}, {
    "fio": "Малиновский Владислав",
    "image": "../img/men34.jpg",
    "sex": 1
}, {
    "fio": "Гребнев Ростислав",
    "image": "../img/men35.jpg",
    "sex": 1
}, {
    "fio": "Кузанов Леонид",
    "image": "../img/men36.jpg",
    "sex": 1
}, {
    "fio": "Ягфаров Серафим",
    "image": "../img/men37.jpg",
    "sex": 1
}, {
    "fio": "Цитников Всеволод",
    "image": "../img/men38.jpg",
    "sex": 1
}, {
    "fio": "Владимиров Артём",
    "image": "../img/men39.jpg",
    "sex": 1
}, {
    "fio": "Банин Александр",
    "image": "../img/men40.jpg",
    "sex": 1
}, {
    "fio": "Капица Кирилл",
    "image": "../img/men41.jpg",
    "sex": 1
}, {
    "fio": "Колбин Клавдий",
    "image": "../img/men42.jpg",
    "sex": 1
}, {
    "fio": "Володин Зиновий",
    "image": "../img/men43.jpg",
    "sex": 1
}, {
    "fio": "Уланов Иван",
    "image": "../img/men44.jpg",
    "sex": 1
}, {
    "fio": "Седых Кирилл",
    "image": "../img/men45.jpg",
    "sex": 1
}, {
    "fio": "Туровский Лев",
    "image": "../img/men46.jpg",
    "sex": 1
}, {
    "fio": "Яснов Ефим",
    "image": "../img/men47.jpg",
    "sex": 1
}, {
    "fio": "Москвин ?Артем",
    "image": "../img/men48.jpg",
    "sex": 1
}, {
    "fio": "Измайлов Герман",
    "image": "../img/men49.jpg",
    "sex": 1
}, {
    "fio": "Цыганов Егор",
    "image": "../img/men50.jpg",
    "sex": 1
}, {
    "fio": "Голумбовский Дмитрий",
    "image": "../img/men51.jpg",
    "sex": 1
}, {
    "fio": "Аничков Кирилл",
    "image": "../img/men52.jpg",
    "sex": 1
}, {
    "fio": "Канаш Степан",
    "image": "../img/men53.jpg",
    "sex": 1
}, {
    "fio": "Клепахов Дмитрий",
    "image": "../img/men54.jpg",
    "sex": 1
}, {
    "fio": "Колесников Анатолий",
    "image": "../img/men55.jpg",
    "sex": 1
}, {
    "fio": "Цветков Леонид",
    "image": "../img/men56.jpg",
    "sex": 1
}, {
    "fio": "Мишин Ефим",
    "image": "../img/men57.jpg",
    "sex": 1
}, {
    "fio": "Борисов Митрофан",
    "image": "../img/men58.jpg",
    "sex": 1
}];


var pluginInfo = {
        "Visitors_Today": "Посетителей сегодня",
        "Visitors_Online": "Посетителей на сайте",
        "Visitors_Buy": "Покупок сегодня",
        "Dostavka": "[fio], г. "+user_city+", сделал(а) заказ на сумму [amount], кол-во 1шт",
        "FastDeliveryTo": "Действует быстрая доставка <br/>в  "+city_delivery,
        "CurrentView": "Сейчас вместе с Вами страницу просматривают [count] чел.",
        "GetDiscount": "Получите скидку!",
        "WithDiscount": "Со скидкой",
        "FillForm": "Заполните форму",
        "OperatorCall": "и наш оператор свяжется с вами в ближайшее время",
        "Btn": "Заказать звонок"        
}

function shuffleArray(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffleArray(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}
peoples = shuffleArray(peoples);
var mainNow = 0;
var cityName = '';
var countryName = '';

function detectCity() {
    var geolocation = ymaps.geolocation;
    cityName = geolocation.city || '';
}

function detectCountry() {
    var geolocation = ymaps.geolocation;
    countryName = geolocation.country;
}
$(function() {
    try {
        ymaps.ready(detectCity);
        ymaps.ready(detectCountry);
    } catch (e) {}
});

function addTopLine(isMobile) {
    isMobile = isMobile ? isMobile : false;
    var allToday = new Date().getHours() * 100 + Math.floor(Math.random() * 1000);
    var now = mainNow != 0 ? mainNow : getRandomInt(45, 150);
    mainNow = now;
    var todayBuy = getRandomInt(50, 100) + new Date().getHours();
    if (allToday <= todayBuy) {
        todayBuy = Math.floor(allToday / 2) + 2;
    }
    var allHeight = isMobile ? 34 : 36;
    var html = '<style>.top-line span.mobile{height: 35px !important;padding-top: 10px !important;font-size: 12px !important;box-sizing: border-box !important;}' +
        'body{padding-top:34px !important;}' +
        '.top-line span{font-family: Arial !important;font-size:18px !important;display: inline-block !important;margin-top: -5px !important;margin-right: 60px;}' +
        '.top-line span *{font-family: Arial !important;font-size:21px !important;}' +
        '.top-line .all-today.mobile{display:none!important;}' +
        '.tm-block-navbar{top: 36px !important;}' +
        '.all-today{background-image: url(/../../_/img/graph.png) !important;height: 40px !important;padding-left: 43px !important;background-repeat: no-repeat !important;background-position: 5px 9px !important;margin-left: 10px !important;display: inline-block !important;padding-top: 8px !important;margin-top: 0 !important;}' +
        '.now{background-image: url(/../../_/img/world.png) !important;height: 40px !important;padding-left: 45px !important;background-repeat: no-repeat !important;background-position: 8px 10px !important;margin-left: 10px !important;display: inline-block !important;padding-top: 8px !important;margin-top: 0 !important;border-left: 0px solid #979a9e !important;}' +
        '.today-buy{background-image: url(/../../_/img/cart.png) !important;height: 40px !important;padding-left: 45px !important;background-repeat: no-repeat !important;background-position: 7px 10px !important;margin-left: 14px !important;display: inline-block !important;padding-top: 8px !important;margin-top: 0 !important;border-left: 0px solid #979a9e !important;}' +
        '.top-line .now.mobile{border-left:0 !important;}</style>' +
        '<div class="top-line" style="opacity: 0.9; overflow: hidden !important;box-sizing: border-box !important; z-index: 99999 !important;height:' + allHeight + 'px !important; text-align:center !important;background: #000 !important; position: fixed !important; width:100% !important;top:0!important; left:0 !important;">' +
        '<div style="font-size: 21px !important;color: #999b9c !important;display:inline-block !important;">' +
        '<span class="now ' + (isMobile ? 'mobile' : '') + '">'+pluginInfo.Visitors_Online+': <strong>' + now + '</strong></span>' +        
        '<span class="all-today ' + (isMobile ? 'mobile' : '') + '">'+pluginInfo.Visitors_Today+': <strong>' + allToday + '</strong></span>' +
        '<span class="today-buy ' + (isMobile ? 'mobile' : '') + '">'+pluginInfo.Visitors_Buy+': <strong>' + todayBuy + '</strong></span>' +
        '</div></div>';
    $(html).appendTo($(document.body));
}


function showTips(bill, bill2) {
    this.bill = bill;
    this.bill2 = bill2;
    this.showItem = 0;
    this.generateHTML = function(image, fio, city, bill, bill2, sex) {
        var top = 50;
        if ($('.freezing-info').length) {
            top = 229;
        }
        var nowMoney = bill;
        if (getRandomInt(0, 1)) {
            nowMoney = bill2;
        }
    nowMoney = nowMoney.replace('сумму','');
    
        return (
            '<div class="notify" style="font-family: \'Roboto\', sans-serif !important; z-index:991000 !important;display: none !important;opacity:0.1 !important;background: #363636 !important;border-radius:10px !important;opacity:0.9; padding:20px !important;width:320px !important;height:90px !important;position:fixed !important;top:' + top + 'px !important;right:20px !important;box-sizing: border-box !important;color: white !important;">' +
            '<img src="' + image + '" width="50" height="50" style="width:50px !important;box-sizing:content-box !important; height: 50px !important; padding-right:20px !important; float: left !important;">' +
            '<div class="notify-text" style="color:#fff;font-size: 14px !important;line-height:normal !important;">'+(pluginInfo.Dostavka.replace('[fio]', fio).replace('[city]',city).replace('[amount]', nowMoney))+' </div>' +
            '</div>'
        );
    };
    this.addItem = function(html) {
        $(html).appendTo($(document.body));
        $('.notify').css('display', 'block');
        $('.notify').animate({
            opacity: 1.0
        }, 'slow');
    };
    this.bindEvent = function() {
        setTimeout(function() {
            $('.notify').animate({
                opacity: 0.1
            }, 'slow', function() {
                $('.notify').css('display', 'none');
                $('.notify').remove();
            });
        }, 8000);
    };
    this.getIntervalAction = function() {
        var self = this;
        return function() {
            var item = peoples[self.showItem];
            if (!item) {
                self.showItem = -1;
                var item = peoples[0];
            }
            self.showItem++;
            var city = cityName;


            if (getRandomInt(0, 1)) {
                city = cityList[getRandomInt(0, 1306)];
            }
            if(city == '') city = user_city;
            var html = self.generateHTML('../../_/img/cart_big.png', item.fio, city, self.bill, self.bill2, item.sex);
            self.addItem(html);
            self.bindEvent();
        }
    };
    setInterval(this.getIntervalAction(), 20000);
}
 

function showDelivery(time) { 

    setTimeout(function() {
        var html = '<div class="delivery-notify" style="line-height:18px !important; font-family: Arial !important; font-size: 12px !important;z-index:991001 !important;position: fixed !important; bottom:8px !important; left: 6px !important; width:265px !important;height: 73px !important;  background: rgb(0, 0, 0) !important; border-radius: 5px !important; opacity: 0.9; color:black; border:1px solid #000 !important; padding: 10px !important;box-sizing: border-box !important;">' +
                '<div class="close-delivery-notify" style="line-height: 13px !important;cursor: pointer !important;width: 15px !important;height: 15px !important;font-size: 22px !important;position: absolute !important;top: 6px !important;right: 6px !important;background: black !important;color: #67696d !important;border: 1px solid #48494b !important;border-radius: 3px !important;">&times;</div>' +
                '<img src="../../_/img/delivery.png" style="    width: 26px !important;height: 26px !important;float: left !important;padding: 0;border: 0 !important;margin: 10px;">' +
                '<span style="color:#c5c9ce;margin-top: 6px;display: block;">'+(pluginInfo.FastDeliveryTo)+
                '</div>';
        $(html).appendTo($(document.body));
            $('.close-delivery-notify').on('click', function() {
                $('.delivery-notify').remove();
        });

    }, 10000);
}


function showSwimmer() {
    var count = mainNow != 0 ? mainNow : getRandomInt(45, 150);
    var bottom = 6;
    if ($('.delivery-notify').length) {
        bottom = 88;
    }
    mainNow = count;
    var html = '<div class="swimmer" style="line-height:18px !important; font-family: Arial !important; font-size: 12px !important;z-index:991001 !important;position: fixed !important; bottom:' + bottom + 'px !important; left: 6px !important; width:265px !important;height: 73px !important;  background: rgb(0, 0, 0) !important; border-radius: 5px !important; opacity: 0.9; color:black; border:1px solid #000 !important; padding: 10px !important;box-sizing: border-box !important;">' +
        '<div class="close-swimmer" style="line-height: 13px !important;cursor: pointer !important;width: 15px !important;height: 15px !important;font-size: 22px !important;position: absolute !important;top: 6px !important;right: 6px !important;background: black !important;color: #67696d !important;border: 1px solid #48494b !important;border-radius: 3px !important;">&times;</div>' +
        '<img src="../../_/img/stat.png" style="    width: 26px !important;height: 26px !important;float: left !important;padding: 0;border: 0 !important;margin: 10px;">' +
        '<span style="color:#c5c9ce;margin-top: 6px;display: block;">'+(pluginInfo.CurrentView.replace('[count]',count) )+' ' + 
        '</div>';

    setTimeout(function() {

        $(html).appendTo($(document.body));
        $('.close-swimmer').on('click', function() {
            $('.swimmer').remove();
        });
    }, 3000);

    setInterval(function() {
        if ($('.delivery-notify').length) {
            $('.swimmer').css('bottom', 88);
        } else {
            $('.swimmer').css('bottom', 6);
        }
    }, 1000);
}

function freezeMoney() {

    var dollar = '45 рублей';

    var html = '<style>' +
        '.freezing-info-packages {font-size: 20px !important;color: #000000 !important;padding-top: 12px !important;z-index: 2 !important;position: relative !important;line-height: 1 !important;}' +
        '.freezing-info-packages span{font-size: 20px !important;color: #000000 !important;padding-top: 12px !important;z-index: 2 !important;position: relative !important;line-height: 1 !important;}' +
        '.freezing-close {position: absolute !important;top: -14px !important;right: 4px !important;width: 20px !important;height: 20px !important;display: block !important;}' +
        '.freezing-info:before {content: "";position: absolute !important;height: 198px !important;width: 280px !important;top: 0 !important;right: 0 !important;margin-top: -26px !important;background: url("../../_/img/buyer-ice.png") no-repeat !important;}' +
        '.freezing-info{font-family: Arial !important; z-index: 991000 !important;color: black !important;width: 329px !important;height: 125px !important;position: fixed !important;background: url("../../_/img/buyer-bg.png") no-repeat !important;box-sizing: border-box !important;padding: 10px 30px !important;top:56px !important;right:0 !important;border: 0 !important;font-size: 100% !important;font: inherit !important;vertical-align: baseline !important;}' +
        '.freezing-info-price {font-size: 22px !important;color: #02aced !important;z-index: 2 !important;position: relative !important;margin-left: 3px !important;}' +
        '.freezing-info-price span{font-size: 22px !important;color: #02aced !important;z-index: 2 !important;position: relative !important;margin-left: 3px !important;}' +
        '.freezing-info-title {font-size: 21px !important;color: #000000 !important;z-index: 2 !important;position: relative !important;text-transform: uppercase !important;line-height: 1.3 !important;}' +
        '.freezing-close:before {-webkit-transform: rotate(45deg) !important;-ms-transform: rotate(45deg) !important;transform: rotate(45deg) !important;}' +
        '.freezing-close:after {-webkit-transform: rotate(-45deg) !important;-ms-transform: rotate(-45deg) !important;transform: rotate(-45deg) !important;}' +
        '.freezing-close:before, ' +
        '.freezing-close:after {content: "";position: absolute !important;width: 100% !important;height: 2px !important;background: #ffffff !important;}' +
        '</style>' +
        '<div class="freezing-info">' +
        '<div class="freezing-info-title">Мы заморозили цену!</div>' +
        '<div class="freezing-info-price">1$ = <span class="dynamic-freezing-info--price">' + dollar + '</span></div>' +
        '<div class="freezing-info-packages">Осталось <span class="packages-count">' + 12 + '</span> штук <br>по старому курсу' +
        '</div>' +
        '<a href="#close" class="freezing-close"></a>' +
        '</div>';
    $(html).appendTo($(document.body));
    $('.freezing-close').on('click', function(e) {
        $('.freezing-info').remove();
        e.preventDefault();
        e.stopPropagation();
    });
}

function comeBacker(time, offerId, newPrice) {

    var startAfter = time;
    var id1 = 'cb_' + Math.round(Math.random() * 999999);
    var id2 = 'cb_' + Math.round(Math.random() * 999999);

    function popup() {

    var div;

    div = $('<div id=' + id1 + ' style="display:none;z-index:11000;position:fixed;background: rgba(0,0,0,0.7); top:0px; left:0px; width: 100%;heighT:100%"></div>');
        var div2 = $('<div id=' + id2 + ' style="display:none;position:fixed;top:100px;width:600px;max-width: 100%;border-radius: 10px; background: white; z-Index:11111; overflow: auto"></div>');
        $(div2).css({left: Math.max(0, $(window).innerWidth() / 2 - 600 / 2)});
        $(window).on('resize', function () {
            $(div2).css({left: Math.max(0, $(window).innerWidth() / 2 - 600 / 2)} );
        });
        $(div2).append('<div style="padding:10px;background: #e91e63; color: white; font-weight:600; font-size: 22px; font-family: Open Sans, sans-serif;overflow: auto"><div style="float:left">'+pluginInfo.GetDiscount+'</div><div id=cb_close style="cursor: pointer; Float:right">&times;</div></div>');
        $(div2).append('<div class="in" style="display:flex;background: white; color: black; font-weight:100; font-size: 14px; font-family: Open Sans, sans-serif;overflow: auto"></div>');
        $(div2).find('.in').append('<div style="background: #fafafa; padding: 20px; display:table-cell; border-right:1px solid #ccc; width: 205px;float:left"><img src="http://cpate.ru/content/img/Offers/' + offerId + '.png" style="border-radius: 50px;max-width: 100%"><Br> <div style="font-family: Open Sans, sans-serif;color:#6f6c6b;font-size: 25px;text-align: center; line-height: 30px;" >'+pluginInfo.WithDiscount+'<b style="display: block;background: #ff3663;color: #ffffff;margin: 10px;padding: 4px;font-size: 25px;border-radius: 3px;">' + newPrice + ' руб.</b></div></div>');
        $(div2).find('.in').append('<div style="background: white;padding: 20px;  display:table-cell;width:400px;font-size: 17px;"><b>'+pluginInfo.FillForm+'</b> '+pluginInfo.OperatorCall+'<div style="margin-bottom: 20px;"></div><form action="../../_/thanks.php" method="post" style="text-align: center;"><input style="font-family: Open Sans; font-size: 20px; padding: 10px; width: 100%; outline: none;margin-top: 10px; border:1px solid #ccc;     box-sizing: border-box;border-radius: 7px;" name="name" placeholder="Ваше имя" required=""> <input style="font-family: Open Sans; font-size: 20px; padding: 10px; width: 100%; outline: none;margin-top: 10px; border:1px solid #ccc;box-sizing: border-box;border-radius: 7px;" name="phone" placeholder="Ваш телефон" required=""> <input type="submit" style=" font-family: Open Sans;padding: 10px;margin-top: 10px;box-sizing: border-box;width: 90%;height: 55px;line-height: 35px;color: #fff;text-align: center; text-decoration: none; text-transform: uppercase;font-size: 20px;font-weight: 700;letter-spacing:0;background: #1ac510;border-radius: 5px;cursor: pointer;border: 0;border-bottom: 3px solid #35a706;" value="'+pluginInfo.Btn+'"></form></div>');
        $('body').append('<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">');
        $('body').append(div);
        $('body').append(div2);
        $(div2).find('[name="clientname"]').focus();
        $('#cb_close').click(function () {
            $('#' + id1).hide();
            $('#' + id2).hide();
            return false;
        });
        $('#' + id1).hide();
        $('#' + id2).hide();
    }
    function show() {
        $('#' + id1).show();
        $('#' + id2).show();
    }
    popup();
    var isAlert = false;
    this.disable = function () {
        isAlert = 1;
    }

    this.fire=  function(){ show()}
    function init() {
        
        var mousePos;

        document.onmousemove = handleMouseMove;
        setInterval(getMousePosition, 50);
        var pos = false;
        var mousePos = false;
        var oldPos = false;
        var isWork = false;

        function handleMouseMove(event) {
            var dot, eventDoc, doc, body, pageX, pageY;

            event = event || window.event;
            mousePos = {
                x: event.clientX,
                y: event.clientY
            };
        }
        function getMousePosition() {
            var pos = mousePos;
            
            if (!pos) {
                return;
            }
            if (!oldPos) {
                oldPos = pos;
                return;
            }
            if (pos.y > 100)
                isWork = 1;
            var distance = Math.sqrt(Math.abs(pos.x - oldPos.x) * Math.abs(pos.x - oldPos.x) + Math.abs(pos.y - oldPos.y) * Math.abs(pos.y - oldPos.y));
            oldPos = pos;
            if (((pos.y < 50 && distance > 280) || distance > 1000 || (pos.y < 40 && distance > 100) || pos.y < 10) && !isAlert) {
                show();
                isAlert = 1;
            }
        }
    }
    var auto_backer = 0;
    if(auto_backer > 0){
       setTimeout(init, 1);
        setTimeout(show, auto_backer*1000);

    }else{
        setTimeout(init, startAfter);
    }

    return this;

}