$(document).ready( function() {

	/*-------------------- MENU --------------------*/
	$('.nav-btn-wrap').on('click', function(){
		$('.top-menu').toggleClass('active');
		$(this).toggleClass('active');
		return false;
	});
	if ($(window).width() < 768 ) {
		$('body').on('click', '.nav-list a', function(){
			$('.top-menu').toggleClass('active');
			$('.nav-btn-wrap').toggleClass('active');
		});
	}
	function searchBlockPosition() {
		if ($(window).scrollTop() > 40) {
			$('.top-menu').addClass('decor');
		}
		else{
			$('.top-menu').removeClass('decor');
		}
	}
	/* ----------- SCROLL SECTION --------------*/
	searchBlockPosition();
	$(window).scroll(function(){
		if ($(window).width() > 767) {
			searchBlockPosition();
		}
	});
	jQuery(window).scroll(function(){
		var $sections = $('section, header');
		$sections.each(function(i,el){
			var top  = $(el).offset().top-400;
			var bottom = top +$(el).height();
			var scroll = $(window).scrollTop();
			var id = $(el).attr('id');
			if( scroll > top && scroll < bottom){
				$('nav a.active').removeClass('active');
				$('nav a[href="#'+id+'"]').addClass('active');
			}
		})
	});
	$("nav").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
		top = $(id).offset().top - 70;
		$('body,html').animate({scrollTop: top}, 600);
	});
	
	/*-------------------- MODAL WINDOW	--------------------*/
	$('.popup-open').on('click', function(){
		$('.overlay').addClass('active');
		$('body').addClass('modal');
		$('.popup').removeClass('active');
		rel=$(this).attr('rel');
		$('.popup-'+rel).addClass('active');
		return false;
	});
	$('.popup-close, .overlay').on('click', function(){
		$('.overlay').removeClass('active');
		$('body').removeClass('modal');
		$('.popup').removeClass('active');
	});
	/*-------------------- PLAY/STOP VIDEO --------------------*/
	$('.close_vid').click(function() {
		$("iframe").each(function() {
			$(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')
		});
	});
	$('.play-one-10').click(function() {
		$(".popup-action10 iframe").each(function() {
			$(this)[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*')
		});
	});
	$('.play-one-11').click(function() {
		$(".popup-action11 iframe").each(function() {
			$(this)[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*')
		});
	});
	$('.play-one-12').click(function() {
		$(".popup-action12 iframe").each(function() {
			$(this)[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*')
		});
	});
	/*-------------------- MODAL CATALOG --------------------*/
	$('.catal-btn').on('click', function(){
		var name_tov = $(this).parents('.item-catal').find('.title').text();
		var type_tov = $(this).parents('.item-catal').find('.type').text();
		var color_tov = $(this).parents('.item-catal').find('input:checked').val();
		$('.popup-callback').find('input[name="name_tov"]').val(name_tov);
		$('.popup-callback').find('input[name="type_tov"]').val(type_tov);
		$('.popup-callback').find('input[name="color_tov"]').val(color_tov);
		console.log(name_tov);
	});
	/*-------------------- TAB --------------------*/
	$('.s-about .tabs-info .tab').on('click', function(){

		$(this).children('.content').show();
		//		$(this).children('.content').toggle(200);	
	});
	/* ------------- SLIDER REVIEWS -------------- */
	$('.slider-reviews').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		speed: 800,
		arrows: true,
		dots: true,
		nextArrow: '<div class="slick-nav arrow-next"><img src="/wp-content/themes/quatroit/img/arrow-r.png" alt="img"></div>',
		prevArrow: '<div class="slick-nav arrow-prev"><img src="/wp-content/themes/quatroit/img/arrow-l.png" alt="img"></div>',
		responsive: [
		{
			breakpoint: 1050,
			settings: {
				slidesToShow: 2
			}
		},
		{
			breakpoint: 700,
			settings: {
				slidesToShow: 1
			}
		}
		]
	});
	/* ------------- SLIDER CATALOG -------------- */
	$('.slider-catalog').slick({
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 800,
		arrows: true,
		dots: false,
		nextArrow: '<div class="slick-nav arrow-next"><img src="/wp-content/themes/quatroit/img/arrow-r.png" alt="img"></div>',
		prevArrow: '<div class="slick-nav arrow-prev"><img src="/wp-content/themes/quatroit/img/arrow-l.png" alt="img"></div>',
	});
	/* ----------- FANCYBOX ----------- */
	$('.fancybox').fancybox({
		loop: false,
		animationEffect: "zoom",
		transitionEffect: "circular"
	});
	/*-------------------- TEXTAREA -------------------*/
	function elasticArea() {
		$('.js-elasticArea').each(function(index, element) {
			var elasticElement = element,
			$elasticElement = $(element),
			initialHeight = initialHeight || $elasticElement.height(),
			delta = parseInt( $elasticElement.css('paddingBottom') ) + parseInt( $elasticElement.css('paddingTop') ) || 0,
			resize = function() {
				$elasticElement.height(initialHeight);
				$elasticElement.height( elasticElement.scrollHeight - delta );
			};

			$elasticElement.on('input change keyup', resize);
			resize();
		});
	};
	/*-------------------- INPUTS -------------------*/
	if ($(window).width() < 767) {
		$('.inp-name').attr('placeholder', 'Ваше Имя');
		$('.inp-email').attr('placeholder', 'Ваш E-mail');
		$('.inp-tel').attr('placeholder', 'Номер телефон');
		$('.inp-textarea').attr('placeholder', 'Сообщение');
	}
	//INIT FUNCTION IN THE VIEW
	elasticArea();
	/*-------------------- QUESTION -------------------*/
	var counter = 1;
	var counter_step = $('.test-wrap .step-wrap').size();
	var remove_class = ('active-step' + counter);
	var add_class = ('active-step' + counter);
	var decor_step = ((1 / counter_step ) * 100);
	var decor_step_int = decor_step;
	$('.test-wrap .step-wrap .decor-steps .decor-line').css('width', (1 / counter_step ) * 100 + '%');
	$('.next-step').on('click', function(){
		var remove_class = ('active-step' + counter);
		$('.test-wrap').removeClass(remove_class);
		counter++;
		decor_step_int = (decor_step_int + decor_step); 
		$('.test-wrap .step-wrap .decor-steps .decor-line').css('width', decor_step_int + '%');
		if( counter == counter_step ){
			$('.test-wrap').addClass('active-last-step');
		}
		else{
			var add_class = ('active-step' + counter);
			$('.test-wrap').addClass(add_class);
		}
		return false;
	});
	/* ------------- FORM ------------------ */
	$(".test-wrap").submit(function(){
		var form =  $(this);
		$.ajax({
			method: "POST",
			url: "/wp-admin/admin-ajax.php",
			dataType: "html",
			data:form.serialize(),
			success: function(data) {
				// $('.popup').removeClass('active');
				$('.popup-action4').addClass('active');
				// $('body').addClass('modal');
				// $('.overlay').addClass('active');
                window.location.href = "http://kilnex.com/success/";
			},
			error: function() {
				alert('Ошибка');
			}
		});
		return false;
	});
	/* ANY */
	$('.wpcf7').addClass('right');
	$('.nav-list li:first-child a').addClass('active');
	var test = $('ul').find('li:first-child').find('input[type="radio"]').click();

/*redirect, if mail send*/
    /*redirect, if mail send*/
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        location = 'http://kilnex.com/success/';
    }, false );

    /*end redirect*/
    if(window.location.href=='http://kilnex.com/success/'){
        setTimeout(function () {
            location = 'http://kilnex.com/';
        },5000)
    }
});

