from flask import Flask, render_template, send_from_directory, request
import smtplib
import json
import sys

reload(sys)
sys.setdefaultencoding('utf8')
app = Flask(__name__)

gmail_user = 'zhbon14@yandex.ru'
gmail_password = '111fff1D'


def send_mail(body):
    sent_from = gmail_user
    to = [gmail_user]
    subject = u'New Lampa Order'
    print(body)
    email_text = u"""\
    From: %s
    To: %s
    Subject: %s

    %s
    """ % (sent_from, ", ".join(to), subject, body)

    try:
        server = smtplib.SMTP_SSL('smtp.yandex.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(sent_from, to, email_text)
        server.close()
        print 'Email sent!'
    except Exception as e:
        raise e
        print 'Something went wrong...'


@app.route('/', methods=['POST', 'GET'])
def hello_world():
    if request.method == 'POST':
        result = json.dumps(request.form.to_dict(flat=False))
        body = json.loads(str(result), encoding='utf-8')
        name = body.get(u'name', [''])[0]
        phone = body.get(u'phone', [''])[0]
        body = name + u'\n' + phone
        send_mail(body)
        return render_template('success.html')
    else:
        return render_template('index.html')


@app.route('/static/<filename>')
def staticb(filename):
    return send_from_directory('static', filename)


if __name__ == '__main__':
    app.run(debug=True)
